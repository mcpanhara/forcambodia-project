class ProductVariant < ApplicationRecord
  #Relationship
  belongs_to :product
  belongs_to :variant
end
