class Prototype < ApplicationRecord
  before_save :update_slug

  has_many :products
  has_many :prototype_properties
  has_many :properties, through: :prototype_properties
  
  belongs_to :user, required: false
  accepts_nested_attributes_for :properties, :prototype_properties
  def update_slug
    self.slug = name.parameterize
  end

  def to_param
    slug
  end
end
