class Product < ApplicationRecord
  before_save :update_slug

  #Relationship
  has_many :product_properties, inverse_of: :product
  has_many :properties, through: :product_properties

  has_many :product_variants, inverse_of: :product
  has_many :variants, through: :product_properties

  belongs_to :user, required: false
  belongs_to :prototype, required: false

  accepts_nested_attributes_for :product_properties, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :product_variants,reject_if: proc { |attributes| attributes['sku'].blank? }
  def update_slug
    self.slug = product_name.parameterize
  end

  def to_param
    slug
  end
end
