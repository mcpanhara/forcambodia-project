class ProductProperty < ApplicationRecord
  #Relationship
  belongs_to :product
  belongs_to :property

  # validates :product_id, uniqueness: { scope: :product_id}
  # validates :property_id, presence: true
end
