class Property < ApplicationRecord
  before_save :update_slug
  #Relationship
  has_many :prototype_properties
  has_many :protoypes, :through => :prototype_properties

  has_many :product_properties
  has_many :products, through: :product_properties
  
  belongs_to :user, required: false
  validates :identifing_name, presence: true

  def update_slug
    self.slug = identifing_name.parameterize
  end

  def to_param
    slug
  end
end
