class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable

  enum role: [:user,:staff,:vip,:admin]
  after_initialize :set_default_role, :if => :new_record?

  #instance method
  def set_default_role
    self.role ||= :user
  end

  #Relationship
  has_many :prototypes
  has_many :properties
  has_many :products
  has_many :variants
end
