class Variant < ApplicationRecord
  before_save :update_slug
  #Relationship
  has_many :product_variants
  has_many :products, through: :product_variants
  
  belongs_to :user, required: false

  def update_slug
    self.slug = sku.parameterize
  end

  def to_param
    slug
  end
end
