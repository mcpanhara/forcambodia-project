class Admin::Changes::PropertiesController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?
  helper_method :all_properties
  before_action :set_product, only: [:edit, :update]

  def edit
    if !@product.product_properties.exists?
      @product.product_properties.build
    end
  end

  def update
    if @product.update_attributes(property_params)
      flash[:notice] = "Successfully updated properties."
      redirect_to admin_product_path(@product.slug)
    else
      render :edit
    end
  end
  private
    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :alert => "You can not access to this page"
        end
      end
    end

    def property_params
      params.required(:product).permit!
    end

    def all_properties
      @all_properties ||= Property.all.map { |p| [p.identifing_name, p.id]}
    end

    def set_product
      @product = Product.find_by_slug(params[:product_id])
    end
end
