class Admin::Multi::VariantsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?
  helper_method :all_variants
  before_action :set_variant, only: [:show,:edit,:update,:destroy]

  def edit
    if !@product.product_variants.exists?
      @product.product_variants.build
    end
  end

  def update
    if @product.update_attributes(variant_params)
      flash[:notice] = "Successfully Updated Variant."
      redirect_to admin_product_path(@product.slug)
    else
      render :edit
    end
  end

  private
    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :alert => "You cannot access to this section"
        end
      end
    end

    def variant_params
      params.require(:product).permit!
    end

    def all_variants
      @all_variants  ||= Variant.all.map { |v| [v.sku, v.id]}
    end

    def set_variant
      @product = Product.find_by_slug(params[:product_id])
    end
end
