class Admin::PropertiesController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?
  before_action :set_property, only: [:show,:edit,:update,:destroy]

  def index
    @properties = Property.all.order(id: :desc)
  end

  def show

  end

  def new
    @property = current_admin_user.properties.build
  end

  def create
    @property = current_admin_user.properties.build(property_params)
    if @property.save
      redirect_to admin_property_path(@property), notice: "Successfully Created"
    else
      flash[:error] = "The property could not be saved"
      render :new
    end
  end

  private
    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "You're not able to access this page"
        end
      end
    end

    def set_property
      @property = Property.find_by_slug(params[:id])
    end

    def property_params
      params.required(:property).permit(:identifing_name,:display_name,:user_id)
    end
end
