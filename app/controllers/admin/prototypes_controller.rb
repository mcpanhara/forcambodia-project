class Admin::PrototypesController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?
  before_action :set_prototype, only: [:show, :edit, :update, :destroy]

  def index
    @prototypes = Prototype.all
  end

  def show

  end

  def new
    @all_properties = Property.all
    if @all_properties.empty?
      flash[:notice] = "You must create property before you create prototype"
      redirect_to new_admin_property_path
    else
      @prototype = current_admin_user.prototypes.build(active: true)
      @prototype.properties.build
    end
  end

  def create
    @prototype = current_admin_user.prototypes.build(prototype_params)
    if @prototype.save
      @prototype.update_attributes(property_ids: params[:prototype][:property_ids])
      redirect_to admin_prototype_path(@prototype), notice: "Successfully Created"
    else
      @all_properties = Property.all
      flash[:error] = "The prototype could not be saved"
      render :new3
    end
  end

  def edit
    @all_properties = Property.all
    @prototype = Prototype.includes(:properties).find_by_slug(params[:id])
  end

  def update
    @prototype = Prototype.find_by_slug(params[:id])
    @prototype.property_ids = params[:prototype][:property_ids]
    if @prototype.update_attributes(prototype_params)
      redirect_to admin_prototype_path(@prototype), notice: "Successfully Updated"
    else
      @all_properties = Property.all
      render :edit
    end
  end

  private
    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "You're not able to access this page"
        end
      end
    end

    def set_prototype
      @prototype = Prototype.find_by_slug(params[:id])
    end

    def prototype_params
      params.require(:prototype).permit(:name, :active, :user_id, property_ids: [])
    end

end
