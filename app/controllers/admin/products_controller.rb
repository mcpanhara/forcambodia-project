class Admin::ProductsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?
  before_action :form_infomation, only: [:new,:create]
  before_action :set_product, only: [:show,:edit,:update,:destroy]

  def index
    @products = Product.all
  end

  def show

  end

  def new
    if @prototypes.empty?
      flash[:notice] = "You must go to create prototype before you create product"
      redirect_to new_admin_prototype_path
    else
      @product = current_admin_user.products.build
      @product.prototype = Prototype.new
    end
  end

  def create
    @product = current_admin_user.products.build(product_params)
    if @product.save
      flash[:notice] = "Successfully Created, You Should Create A Variant For The Product"
      redirect_to edit_admin_product_path(@product)
    else
      flash[:error] = "The product could not be saved"
      render :new
    end
  end

  private
    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "You're not able to access this page"
        end
      end
    end

    def set_product
      @product = Product.find_by_slug(params[:id])
    end

    def product_params
      params.required(:product).permit(:product_name,:product_keywords,:permalink,:meta_keywords,:meta_description,:user_id,:prototype_id)
    end

    def form_infomation
      @prototypes = Prototype.all.map{|pt| [pt.name, pt.id]}
    end
end
