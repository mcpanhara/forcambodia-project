class CreatePrototypeProperties < ActiveRecord::Migration[5.1]
  def change
    create_table :prototype_properties do |t|
      t.integer :prototype_id, :null => false
      t.integer :property_id, :null => false

      t.timestamps
    end
    add_index :prototype_properties, :prototype_id
    add_index :prototype_properties, :property_id
  end
end
