class CreateProductVariants < ActiveRecord::Migration[5.1]
  def change
    create_table :product_variants do |t|
      t.integer :product_id
      t.integer :variant_id
      t.string :sku, :null => false, :unique => true
      t.string :name, :null => false
      t.decimal :price, :null => false, :precision => 8, :scale => 2, :default => 0.0
      t.decimal :cost, :null => false, :precision => 8, :scale => 2, :default => 0.0
      t.datetime :delated_at
      t.boolean :master,    :default => false, :null => false
      t.integer :count_on_hand,               :default => 0, :null => false
      t.integer :count_pending_to_customer,   :default => 0, :null => false
      t.integer :count_pending_from_supplier, :default => 0, :null => false
      t.timestamps
    end
    add_index :product_variants, :sku
  end
end
