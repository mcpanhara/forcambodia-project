class CreateProductProperties < ActiveRecord::Migration[5.1]
  def change
    create_table :product_properties do |t|
      t.integer :product_id, :null => false
      t.integer :property_id, :null => false
      t.integer :position
      t.text :description, :null => false

      t.timestamps
    end
  end
end
