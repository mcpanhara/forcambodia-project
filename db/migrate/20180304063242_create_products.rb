class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product_name,           :null => false
      t.text :descritpion
      t.text :product_keywords
      t.string :permalink,               :null => false
      t.datetime :available_at
      t.datetime :deleted_at
      t.string :meta_keywords
      t.string :meta_description
      t.boolean :featured,               :default => false
      t.integer :user_id

      t.timestamps
    end
    add_index :products, :product_name
    add_index :products, :deleted_at
    add_index :products, :permalink, :unique => true
  end
end
