class AddPrototypeIdToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :prototype_id, :integer
  end
end
