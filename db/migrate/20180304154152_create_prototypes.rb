class CreatePrototypes < ActiveRecord::Migration[5.1]
  def change
    create_table :prototypes do |t|
      t.string :name, :null => false
      t.boolean :active, :null => false, default: true
      t.integer :user_id
      t.timestamps
    end
  end
end
