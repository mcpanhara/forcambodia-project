class CreateVariants < ActiveRecord::Migration[5.1]
  def change
    create_table :variants do |t|
      t.integer :product_id, :null => false
      t.integer :user_id, :null => false
      t.timestamps
    end
    add_index :variants, :product_id
  end
end
