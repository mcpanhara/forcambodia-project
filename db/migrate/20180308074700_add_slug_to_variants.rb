class AddSlugToVariants < ActiveRecord::Migration[5.1]
  def change
    add_column :variants, :slug, :string
  end
end
