class AddSlugToPrototypes < ActiveRecord::Migration[5.1]
  def change
    add_column :prototypes, :slug, :string
  end
end
