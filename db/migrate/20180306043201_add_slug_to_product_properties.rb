class AddSlugToProductProperties < ActiveRecord::Migration[5.1]
  def change
    add_column :product_properties, :slug, :string
  end
end
