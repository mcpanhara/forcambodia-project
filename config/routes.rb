Rails.application.routes.draw do
  # devise_for :users
  namespace :admin do
    root "dashboard#index"
    get "dashboard" => "dashboard#index", :as => :dashboard
    devise_scope :user do
      get "/sign_in" => "devise/sessions#new" # custom path to login/sign_in
      get "/sign_up" => "devise/registrations#new", as: "new_user_registration" # custom path to sign_up/registration

    end

    devise_for :users, controllers: { sessions: 'admin/users/sessions'}, :passwords => 'admin/users/passwords', :skip => [:registrations]

    as :user do
      get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
      put 'users' => 'devise/registrations#update', :as => 'user_registration'
      patch 'users' => 'devise/registrations#update', :as => 'update_user_registration'
    end

    resources :blogs
    resources :products
    resources :properties
    resources :prototypes

    namespace :changes do
      resources :products do
        resource :property,only: [:edit, :update]
      end
    end
    
    namespace :multi do
      resources :products do
        resource :variant, only: [:edit, :update]
      end
    end


  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
